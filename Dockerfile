FROM rust:1.72.1 as builder

WORKDIR /app

COPY . .

RUN cargo build --release

# Production stage
FROM debian:stable-slim
WORKDIR /usr/local/bin
COPY --from=builder /app/target/release/web-cloud .
EXPOSE 8080
CMD ["./web-cloud"]


