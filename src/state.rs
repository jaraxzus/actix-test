use std::{collections::HashMap, sync::Arc};

use tokio::sync::Mutex;

pub struct GuardedAppState {
    pub state: Arc<Mutex<State>>,
}
#[derive(Default)]
pub struct State {
    pub tasks: HashMap<u64, String>,
    pub counter: u64,
}
