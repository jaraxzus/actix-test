use actix_web::{web, App, HttpServer};
use std::sync::Arc;
use tokio::sync::Mutex;
mod state;
use state::{GuardedAppState, State};

mod api;
use api::task_api::*;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    println!("Starting job");
    let data = web::Data::new(GuardedAppState {
        state: Arc::new(Mutex::new(State::default())),
    });
    HttpServer::new(move || {
        App::new()
            .app_data(data.clone())
            .service(get_all_tasks)
            .service(add_task)
            .service(get_task)
            .service(update_task)
            .service(delete_task)
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
