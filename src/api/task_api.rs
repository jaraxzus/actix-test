use crate::state::GuardedAppState;

use actix_web::{delete, error::Error, get, patch, post, web, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Deserialize, Serialize)]
struct AddTask {
    body: String,
}

#[get("/tasks")]
async fn get_all_tasks(data: web::Data<GuardedAppState>) -> impl Responder {
    let json_string: HashMap<u64, String> = data.state.lock().await.tasks.clone();
    HttpResponse::Ok().json(json_string)
}

#[post("/tasks")]
async fn add_task(
    task: web::Json<AddTask>,
    state: web::Data<GuardedAppState>,
) -> Result<impl Responder, Error> {
    let mut state = state.state.lock().await;
    let counter = state.counter;
    let body = task.body.to_string();
    Ok(match state.tasks.insert(counter, body) {
        Some(_) => HttpResponse::BadRequest().await?,
        None => {
            state.counter += 1;
            HttpResponse::Created().await?
        }
    })
}

#[get("/task/{task_id}")]
async fn get_task(
    path: web::Path<u64>,
    data: web::Data<GuardedAppState>,
) -> Result<impl Responder, Error> {
    let task_id = path.into_inner();
    let tasks = &data.state.lock().await.tasks;
    Ok(match tasks.get_key_value(&task_id) {
        Some(task) => HttpResponse::Ok().json(task),
        None => HttpResponse::NotFound().await?,
    })
}

#[patch("/task/{task_id}")]
async fn update_task(
    path: web::Path<u64>,
    task: web::Json<AddTask>,
    data: web::Data<GuardedAppState>,
) -> Result<impl Responder, Error> {
    let task_id = path.into_inner();
    let task_body = task.body.to_string();
    Ok(
        match data.state.lock().await.tasks.insert(task_id, task_body) {
            Some(_) => HttpResponse::Ok().await?,
            None => HttpResponse::NotFound().await?,
        },
    )
}

#[delete("/task/{task_id}")]
async fn delete_task(
    path: web::Path<u64>,
    data: web::Data<GuardedAppState>,
) -> Result<impl Responder, Error> {
    let task_id = path.into_inner();
    Ok(match data.state.lock().await.tasks.remove(&task_id) {
        Some(_) => HttpResponse::Ok().await?,
        None => HttpResponse::NotFound().await?,
    })
}

#[cfg(test)]
mod tests {
    use std::sync::Arc;

    use actix_web::{
        http::{header::ContentType, StatusCode},
        test, App,
    };
    use serde_json::json;
    use tokio::sync::Mutex;

    use super::*;

    fn get_data() -> web::Data<GuardedAppState> {
        web::Data::new(GuardedAppState {
            state: Arc::new(Mutex::new(crate::state::State::default())),
        })
    }

    #[actix_web::test]
    async fn test_tasks_get() {
        let app_data = get_data();
        let app =
            test::init_service(App::new().app_data(app_data.clone()).service(get_all_tasks)).await;
        let req = test::TestRequest::get()
            .uri("/tasks")
            .insert_header(ContentType::plaintext())
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert!(resp.status().is_success());
    }

    #[actix_web::test]
    async fn test_tasks_post() {
        let app_data = get_data();
        let app = test::init_service(App::new().app_data(app_data.clone()).service(add_task)).await;
        let data = HashMap::new().insert("body", "test task1");
        let req = test::TestRequest::post()
            .uri("/tasks")
            .data(data)
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert!(resp.status().is_client_error());
    }
    #[actix_web::test]
    async fn test_task_get() {
        let app_data = get_data();
        let state = &app_data.state;
        state
            .lock()
            .await
            .tasks
            .insert(0, "test = task".to_string());
        let app = test::init_service(App::new().app_data(app_data.clone()).service(get_task)).await;
        let req = test::TestRequest::get().uri("/task/0").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), StatusCode::OK);
        let req = test::TestRequest::get().uri("/task/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), StatusCode::NOT_FOUND);
    }
    #[actix_web::test]
    async fn test_patch_task() {
        let app_data = get_data();
        let state = &app_data.state;
        state.lock().await.tasks.insert(0, "test task".to_string());
        let app =
            test::init_service(App::new().app_data(app_data.clone()).service(update_task)).await;
        let req = test::TestRequest::patch()
            .uri("/task/0")
            // HashMap::new().insert("body", "test")
            .set_json(json!({"body": "test"}))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), StatusCode::OK);

        let req = test::TestRequest::patch()
            .uri("/task/2")
            .set_json(json!({"body": "test"}))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), StatusCode::NOT_FOUND);

        let req = test::TestRequest::patch()
            .uri("/task/2")
            .data(HashMap::new().insert("body", "test"))
            .to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), StatusCode::BAD_REQUEST)
    }
    #[actix_web::test]
    async fn test_task_delet() {
        let app_data = get_data();
        let mut tasks = HashMap::new();
        tasks.insert(0, "test task".to_string());
        tasks.insert(1, "test task 1".to_string());
        app_data.state.lock().await.tasks = tasks;
        let app = test::init_service(
            App::new()
                .app_data(app_data.clone())
                .service(delete_task)
                .service(get_task),
        )
        .await;
        let req = test::TestRequest::delete().uri("/task/0").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), StatusCode::OK);
        let req = test::TestRequest::get().uri("/task/1").to_request();
        let resp = test::call_service(&app, req).await;
        assert_eq!(resp.status(), StatusCode::OK)
    }
}
